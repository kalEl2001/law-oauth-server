package models

type Account struct {
	FullName string `json:"full_name" form:"full_name"`
	Npm      string `json:"npm" form:"npm"`
	Username string `json:"username" form:"username"`
}

type Client struct {
	ClientId     string
	ClientName   string
	ClientSecret string
}

type Session struct {
	Username     string `json:"username" form:"username"`
	ClientId     string `json:"client_id form:"client_id"`
	RefreshToken string `json:"refresh_token" form:"refresh_token"`
}

type RegisterAccountRequest struct {
	Username string `json:"username" form:"username"`
	Password string `json:"password" form:"password"`
	FullName string `json:"full_name" form:"full_name"`
	Npm      string `json:"npm" form:"npm"`
}

type RegisterClientRequest struct {
	ClientName string `json:"client_name" form:"client_name"`
}

type RegisterClientResponse struct {
	ClientName   string `json:"client_name" form:client_name"`
	ClientId     string `json:"client_id" form:"client_id"`
	ClientSecret string `json:"client_secret" form:"client_secret"`
}

type GetTokenRequest struct {
	Username     string `json:"username" form:"username"`
	Password     string `json:"password" form:"password"`
	GrantType    string `json:"grant_type" form:"grant_type"`
	ClientId     string `json:"client_id" form:"client_id"`
	ClientSecret string `json:"client_secret" form:"client_secret"`
}

type SessionResponse struct {
	AccessToken  string  `json:"access_token" form:"access_token"`
	ExpiresIn    int     `json:"expires_in" form:"expires_in"`
	TokenType    string  `json:"token_type" form:"token_type"`
	Scope        *string `json:"scope" form:"scope"`
	RefreshToken string  `json:"refresh_token" form:"refresh_token"`
}

type ResourceResponse struct {
	AccessToken  string `json:"access_token" form:"access_token"`
	ClientId     string `json:"client_id" form:"client_id"`
	Username     string `json:"user_id" form:"user_id"`
	FullName     string `json:"full_name" form:"full_name"`
	Npm          string `json:"npm" form:"npm"`
	Expires      *int   `json:"expires" form:"expires"`
	RefreshToken string `json:"refresh_token" form:"refresh_token"`
}
