package main

import (
	"crypto/sha1"
	"encoding/hex"
	"encoding/json"
	"log"
	"math/rand"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis"
	_ "github.com/joho/godotenv/autoload"
	"gitlab.com/kalEl2001/law-oauth-server/models"
	"golang.org/x/crypto/bcrypt"
)

var dbAccount *redis.Client
var dbClient *redis.Client
var dbSession *redis.Client
var dbRefresh *redis.Client
var dbAccountDetails *redis.Client
var sessionExpireTime int = 300

func initDb() {
	addr := os.Getenv("REDIS_HOST") + ":" + os.Getenv("REDIS_PORT")
	pass := os.Getenv("REDIS_PASSWORD")

	dbAccount = redis.NewClient(&redis.Options{
		Addr:     addr,
		Password: pass,
		DB:       0,
	})

	dbClient = redis.NewClient(&redis.Options{
		Addr:     addr,
		Password: pass,
		DB:       1,
	})

	dbSession = redis.NewClient(&redis.Options{
		Addr:     addr,
		Password: pass,
		DB:       2,
	})

	dbRefresh = redis.NewClient(&redis.Options{
		Addr:     addr,
		Password: pass,
		DB:       3,
	})

	dbAccountDetails = redis.NewClient(&redis.Options{
		Addr:     addr,
		Password: pass,
		DB:       4,
	})
}

func hashPassword(password string) (string, error) {
	hash, err := bcrypt.GenerateFromPassword([]byte(password), 14)

	return string(hash), err
}

func verifyUser(username string, password string) bool {
	expectedPasswordHash, err := dbAccount.Get(username).Result()
	if err != nil {
		return false
	}

	checkPassErr := bcrypt.CompareHashAndPassword([]byte(expectedPasswordHash), []byte(password))

	return checkPassErr == nil
}

func verifyClient(clientId string, clientSecret string) bool {
	clientJson, err := dbClient.Get(clientId).Result()

	if err != nil {
		return false
	}

	var clientData models.Client
	err = json.Unmarshal([]byte(clientJson), &clientData)
	if err != nil {
		log.Println(err)
	}
	expectedClientSecret := clientData.ClientSecret

	return expectedClientSecret == clientSecret
}

func verifySession(token string) bool {
	_, err := dbSession.Get(token).Result()
	return err == nil
}

func verifyRefreshToken(token string) bool {
	_, err := dbRefresh.Get(token).Result()
	return err == nil
}

func ping(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"message": "Pong",
		"time":    time.Now(),
	})
}

func registerAccount(c *gin.Context) {
	var req models.RegisterAccountRequest
	if err := c.Bind(&req); err != nil {
		c.JSON(400, gin.H{
			"message": "Missing argument(s)",
		})
		return
	}
	passwordHash, err := hashPassword(req.Password)
	if err != nil {
		c.JSON(500, gin.H{
			"message": "Error while hashing password",
		})
		return
	}
	dbAccount.Set(req.Username, passwordHash, 0)

	accountDetails := models.Account{
		FullName: req.FullName,
		Npm:      req.Npm,
		Username: req.Username,
	}

	marshaledAccountDetails, err := json.Marshal(accountDetails)
	if err != nil {
		c.JSON(500, gin.H{
			"message": "Error while saving registration",
		})
		return
	}

	dbAccountDetails.Set(req.Username, marshaledAccountDetails, 0)

	c.JSON(200, accountDetails)
}

func registerClient(c *gin.Context) {
	var req models.RegisterClientRequest
	if err := c.Bind(&req); err != nil {
		c.JSON(400, gin.H{
			"message": "Missing argument(s)",
		})
		return
	}
	if req.ClientName == "" {
		c.JSON(400, gin.H{
			"message": "Missing argument(s)",
		})
		return
	}
	clientId := rand.Intn(8999) + 1000
	_, err := dbClient.Get(strconv.Itoa(clientId)).Result()
	isAvailable := err != nil
	for !isAvailable {
		clientId = rand.Intn(8999) + 1000
		_, err = dbClient.Get(strconv.Itoa(clientId)).Result()
		isAvailable = err != nil
	}
	clientSecret := rand.Intn(8999) + 1000
	client := models.Client{
		ClientName:   req.ClientName,
		ClientId:     strconv.Itoa(clientId),
		ClientSecret: strconv.Itoa(clientSecret),
	}

	marshaledClient, err := json.Marshal(client)
	if err != nil {
		c.JSON(500, gin.H{
			"message": "Error while saving client",
		})
		return
	}
	err = dbClient.Set(strconv.Itoa(clientId), marshaledClient, 0).Err()
	if err != nil {
		c.JSON(500, gin.H{
			"message": "Error while saving client",
		})
		return
	}
	c.JSON(200, client)
}

func getToken(c *gin.Context) {
	var req models.GetTokenRequest
	if err := c.Bind(&req); err != nil {
		c.JSON(400, gin.H{
			"message": "Missing argument(s)",
		})
		return
	}
	if !verifyClient(req.ClientId, req.ClientSecret) {
		c.JSON(401, gin.H{
			"error":             "invalid_request",
			"Error_description": "ada kesalahan masbro!",
		})
		return
	}
	if !verifyUser(req.Username, req.Password) {
		c.JSON(401, gin.H{
			"error":             "invalid_request",
			"Error_description": "ada kesalahan masbro!",
		})
		return
	}
	if req.GrantType != "password" {
		c.JSON(401, gin.H{
			"error":             "invalid_request",
			"Error_description": "ada kesalahan masbro!",
		})
		return
	}
	tokenString := time.Now().String() + req.Username + req.ClientId
	hash := sha1.New()
	hash.Write([]byte(tokenString))
	hashedToken := hash.Sum(nil)

	refreshTokenString := time.Now().String() + req.Username + req.ClientId + "refresh"
	hash.Write([]byte(refreshTokenString))
	hashedRefreshToken := hash.Sum(nil)

	session := models.Session{
		Username:     req.Username,
		ClientId:     req.ClientId,
		RefreshToken: hex.EncodeToString(hashedRefreshToken),
	}

	sessionByte, err := json.Marshal(session)
	if err != nil {
		c.JSON(500, gin.H{
			"message": "Error while saving session",
		})
		return
	}

	dbSession.Set(hex.EncodeToString(hashedToken), sessionByte, time.Second*time.Duration(sessionExpireTime))
	dbRefresh.Set(hex.EncodeToString(hashedRefreshToken), hex.EncodeToString(hashedToken), time.Second*time.Duration(sessionExpireTime))

	sessionResponse := models.SessionResponse{
		AccessToken:  hex.EncodeToString(hashedToken),
		ExpiresIn:    sessionExpireTime,
		TokenType:    "Bearer",
		RefreshToken: hex.EncodeToString(hashedRefreshToken),
	}

	c.JSON(200, sessionResponse)
}

func getResource(c *gin.Context) {
	authorization := c.Request.Header["Authorization"]
	if authorization[0][0:6] == "Bearer" {
		token := authorization[0][7:]
		if verifySession(token) {
			sessionJson, err := dbSession.Get(token).Result()
			if err != nil {
				c.JSON(500, gin.H{
					"error":             "internal_server_error",
					"Error_description": "ada kesalahan masbro!",
				})
				return
			}

			var sessionData models.Session

			err = json.Unmarshal([]byte(sessionJson), &sessionData)
			if err != nil {
				c.JSON(500, gin.H{
					"error":             "internal_server_error",
					"Error_description": "ada kesalahan masbro!",
				})
				return
			}

			accountDetailsJson, err := dbAccountDetails.Get(sessionData.Username).Result()
			if err != nil {
				c.JSON(500, gin.H{
					"error":             "internal_server_error",
					"Error_description": "ada kesalahan masbro!",
				})
				return
			}

			var accountDetailsData models.Account

			err = json.Unmarshal([]byte(accountDetailsJson), &accountDetailsData)
			if err != nil {
				c.JSON(500, gin.H{
					"error":             "internal_server_error",
					"Error_description": "ada kesalahan masbro!",
				})
				return
			}
			resourceResponse := models.ResourceResponse{
				AccessToken:  token,
				ClientId:     sessionData.ClientId,
				Username:     sessionData.Username,
				FullName:     accountDetailsData.FullName,
				Npm:          accountDetailsData.Npm,
				RefreshToken: sessionData.RefreshToken,
			}
			c.JSON(200, resourceResponse)
		} else {
			c.JSON(401, gin.H{
				"error":             "invalid_token",
				"error_description": "Token Salah masbroww",
			})
			return
		}
	} else {
		c.JSON(401, gin.H{
			"error":             "invalid_request",
			"Error_description": "ada kesalahan masbro!",
		})
		return
	}
}

func refreshToken(c *gin.Context) {
	authorization := c.Request.Header["Authorization"]
	if authorization[0][0:6] == "Bearer" {
		token := authorization[0][7:]
		if verifyRefreshToken(token) {
			oldToken, err := dbRefresh.Get(token).Result()
			if err != nil {
				c.JSON(500, gin.H{
					"error":             "internal_server_error",
					"Error_description": "ada kesalahan masbro!",
				})
				return
			}

			sessionJson, err := dbSession.Get(oldToken).Result()
			if err != nil {
				c.JSON(500, gin.H{
					"error":             "internal_server_error",
					"Error_description": "ada kesalahan masbro!",
				})
				return
			}

			dbSession.Del(oldToken)

			var sessionData models.Session

			err = json.Unmarshal([]byte(sessionJson), &sessionData)
			if err != nil {
				c.JSON(500, gin.H{
					"error":             "internal_server_error",
					"Error_description": "ada kesalahan masbro!",
				})
				return
			}

			newTokenString := time.Now().String() + sessionData.Username + sessionData.ClientId
			hash := sha1.New()
			hash.Write([]byte(newTokenString))
			newHashedToken := hash.Sum(nil)

			newRefreshTokenString := time.Now().String() + sessionData.Username + sessionData.ClientId + "refresh"
			hash.Write([]byte(newRefreshTokenString))
			newHashedRefreshToken := hash.Sum(nil)

			session := models.Session{
				Username:     sessionData.Username,
				ClientId:     sessionData.ClientId,
				RefreshToken: hex.EncodeToString(newHashedRefreshToken),
			}

			sessionByte, err := json.Marshal(session)
			if err != nil {
				c.JSON(500, gin.H{
					"message": "Error while saving session",
				})
				return
			}

			dbSession.Set(hex.EncodeToString(newHashedToken), sessionByte, time.Second*time.Duration(sessionExpireTime))
			dbRefresh.Set(hex.EncodeToString(newHashedRefreshToken), hex.EncodeToString(newHashedToken), time.Second*time.Duration(sessionExpireTime))

			sessionResponse := models.SessionResponse{
				AccessToken:  hex.EncodeToString(newHashedToken),
				ExpiresIn:    sessionExpireTime,
				TokenType:    "Bearer",
				RefreshToken: hex.EncodeToString(newHashedRefreshToken),
			}

			c.JSON(200, sessionResponse)
		} else {
			c.JSON(401, gin.H{
				"error":             "invalid_token",
				"error_description": "Token Salah masbroww",
			})
			return
		}
	} else {
		c.JSON(401, gin.H{
			"error":             "invalid_request",
			"Error_description": "ada kesalahan masbro!",
		})
		return
	}
}

func main() {
	log.Println("LAW OAuth Server is starting")

	log.Println("Initializing DB")
	initDb()

	_, err := dbAccount.Ping().Result()
	log.Println("Error for account DB: ", err)
	_, err = dbClient.Ping().Result()
	log.Println("Error for client DB: ", err)
	_, err = dbSession.Ping().Result()
	log.Println("Error for session DB: ", err)
	_, err = dbRefresh.Ping().Result()
	log.Println("Error for refresh DB: ", err)
	_, err = dbAccountDetails.Ping().Result()
	log.Println("Error for account details DB: ", err)

	log.Println("Initializing Router")
	r := gin.Default()

	defaultAPI := r.Group("/oauth")
	{
		defaultAPI.GET("/ping", ping)
		defaultAPI.POST("/account/register", registerAccount)
		defaultAPI.POST("/client/register", registerClient)
		defaultAPI.POST("/token", getToken)
		defaultAPI.POST("/resource", getResource)
		defaultAPI.POST("/refresh", refreshToken)
	}

	r.Run(":55592")
}
